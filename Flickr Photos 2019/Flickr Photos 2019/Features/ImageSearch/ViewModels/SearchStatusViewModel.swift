//
//  SearchStatusViewModel.swift
//  Flickr Photos 2019
//
//  Created by Mitchell Currie on 10/1/20.
//  Copyright © 2020 Mitchell Currie. All rights reserved.
//

import Foundation

struct SearchStatusViewModel {
    let activityText: String
    let showsActivity: Bool
}
