// PhotoSearchResponse.swift

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let photoSearchResponse = try? newJSONDecoder().decode(PhotoSearchResponse.self, from: jsonData)

import Foundation

// MARK: - PhotoSearchResponse
struct PhotoSearchResponse: Codable {
    let photos: Photos
    let stat: String
}
