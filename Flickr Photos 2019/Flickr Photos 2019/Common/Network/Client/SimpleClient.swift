//
//  SimpleClient.swift
//  Flickr Photos 2019
//
//  Created by Mitchell Currie on 5/1/20.
//  Copyright © 2020 Mitchell Currie. All rights reserved.
//

import Foundation

class SimpleClient: APIClient {
    var endpoint: Endpoint
    let apiKey = "67fd6b247449a18aa0e039770373b8ab"
    let apiSecret = "1c9f9e61113e7d9f"
    let perPage = 96  // divisible by 2,3,4 so good for differing column layouts

    init(endpoint: Endpoint) {
        self.endpoint = endpoint
    }
    func searchPhotos(searchTerm: String, page: Int, completionHandler: @escaping (Result<PhotoSearchResponse, NetworkError>) -> Void) {
        
        guard let url = URL(string: "\(endpoint.host)/?method=flickr.photos.search&api_key=\(apiKey)&text=\(searchTerm)&page=\(page)&format=json&nojsoncallback=1&per_page=\(perPage)") else {
            completionHandler(Result<PhotoSearchResponse, NetworkError>.failure(NetworkError.requestInvalid))
            return
        }
        
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            // Parsing may require work, unlike downloading which iOS did for us... we'll need to slip off the main thread to parse the response
            DispatchQueue.global(qos: .userInitiated).async {
                guard let data = data, error == nil else {
                    DispatchQueue.main.async {
                        completionHandler(Result<PhotoSearchResponse, NetworkError>.failure(NetworkError.badResponse))
                    }
                    return
                }
                // Use the Swift4 Codable and JSONDecoder to do the heavy lifting for us
                do {
                    let feed = try JSONDecoder().decode(PhotoSearchResponse.self, from: data)
                    DispatchQueue.main.async {
                        completionHandler(Result<PhotoSearchResponse, NetworkError>.success(feed))
                    }
                } catch let jsonError {
                    print(jsonError)
                    DispatchQueue.main.async {
                        completionHandler(Result<PhotoSearchResponse, NetworkError>.failure(NetworkError.badResponse))
                    }
                }
            }
        }).resume()
    }
    
    
}
