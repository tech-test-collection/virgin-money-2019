//
//  PhotoItem.swift
//  Flickr Photos 2019
//
//  Created by Mitchell Currie on 5/1/20.
//  Copyright © 2020 Mitchell Currie. All rights reserved.
//

import Foundation

struct PhotoItem {
    let title: String
    let imageURL: String
}

extension PhotoItem {
    init(photo: Photo) {
        title = photo.title
        imageURL = "https://farm\(photo.farm).staticflickr.com/\(photo.server)/\(photo.id)_\(photo.secret)_n.jpg"
    }
}
