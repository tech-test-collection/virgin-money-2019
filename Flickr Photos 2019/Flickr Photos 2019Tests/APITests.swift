//
//  APITests.swift
//  Flickr Photos 2019Tests
//
//  Created by Mitchell Currie on 6/1/20.
//  Copyright © 2020 Mitchell Currie. All rights reserved.
//

import XCTest

class APITests: XCTestCase {
    func testSuccess() {
        setupStubsDefault()
        
        let apiClient = SimpleClient(endpoint: .production)
        let completionExpectation = expectation(description: "testAPI")
        
        apiClient.searchPhotos(searchTerm: "alligator", page: 1) { result in
            completionExpectation.fulfill()
            switch result {
            case .success(let response):
                XCTAssertEqual(response.photos.photo[0].id, "49338033191")
            case .failure(_):
                XCTFail("Success was expected")
            }
        }
        
        waitForExpectations(timeout: .waitExpectation) { error in
            XCTAssertNil(error, "Time out error")
        }
    }
    
    func testError() {
        setupStubError()
        
        let apiClient = SimpleClient(endpoint: .production)
        let completionExpectation = expectation(description: "testAPI")
        
        apiClient.searchPhotos(searchTerm: "alligator", page: 1) { result in
            completionExpectation.fulfill()
            switch result {
            case .success(_):
                XCTFail("failure was expected")
            case .failure(let error):
               XCTAssertNotNil(error)
            }
        }
        
        waitForExpectations(timeout: .waitExpectation) { error in
            XCTAssertNil(error, "Time out error")
        }
    }
}
