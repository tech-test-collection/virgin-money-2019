//
//  PhotoCollectionViewCell.swift
//  Flickr Photos 2019
//
//  Created by Mitchell Currie on 5/1/20.
//  Copyright © 2020 Mitchell Currie. All rights reserved.
//

import UIKit
import Kingfisher


class PhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}

extension PhotoCollectionViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setup(item: PhotoItem) {
        titleLabel.text = item.title
        imageView.image = nil
        guard let imageURL = URL(string: item.imageURL) else { return }
        imageView.kf.setImage(with: imageURL)
    }
}
