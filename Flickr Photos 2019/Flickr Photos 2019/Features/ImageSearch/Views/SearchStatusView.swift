//
//  SearchStatusView.swift
//  Flickr Photos 2019
//
//  Created by Mitchell Currie on 6/1/20.
//  Copyright © 2020 Mitchell Currie. All rights reserved.
//

import UIKit

class SearchStatusView: UICollectionReusableView {
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
}

extension SearchStatusView {
    func setup(viewModel: SearchStatusViewModel) {
        statusLabel.text = viewModel.activityText
        if viewModel.showsActivity {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
}
