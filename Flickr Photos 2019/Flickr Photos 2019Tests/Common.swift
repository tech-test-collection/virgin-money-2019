//
//  Common.swift
//  Flickr Photos 2019Tests
//
//  Created by Mitchell Currie on 6/1/20.
//  Copyright © 2020 Mitchell Currie. All rights reserved.
//

import XCTest
import OHHTTPStubs

extension TimeInterval {
    static let dispatch = 0.1
    static let waitExpectation = 1.0
}

extension XCTestCase {
    func setupStubsDefault() {
        stub(condition: isMethodGET()) { request in
            var stubName = "Stub1"
            
            if request.url?.absoluteString.contains("page=2") ?? false {
                stubName = "Stub2"
            }
            
            guard let stubURL = Bundle(for: type(of: self)).url(forResource: stubName, withExtension: "json") else {
                fatalError("stub not found")
            }
            return OHHTTPStubsResponse(fileURL: stubURL, statusCode: 200, headers: nil)
        }
    }
    
    func setupStubError() {
        stub(condition: isMethodGET()) { _ in
            return OHHTTPStubsResponse(data: Data(), statusCode: 501, headers:nil)
        }
    }
}
