//
//  APIClient.swift
//  Flickr Photos 2019
//
//  Created by Mitchell Currie on 5/1/20.
//  Copyright © 2020 Mitchell Currie. All rights reserved.
//

import Foundation

enum Endpoint {
    case production
}

protocol APIClient {
    // fetches the photos
    func searchPhotos(searchTerm: String, page: Int, completionHandler: @escaping (Result<PhotoSearchResponse, NetworkError>) -> Void)
}

extension Endpoint {
    var host: String {
        return "https://www.flickr.com/services/rest"
    }
}
