//
//  ViewController.swift
//  Flickr Photos 2019
//
//  Created by Mitchell Currie on 5/1/20.
//  Copyright © 2020 Mitchell Currie. All rights reserved.
//

import UIKit

extension PhotosViewController {
    fileprivate enum Nib: String {
        case photo = "StandardPhoto"
        case statusFooter = "StatusFooter"
        var nib: UINib {
            return UINib(nibName: self.rawValue, bundle: nil)
        }
        var name: String {
            return self.rawValue
        }
    }
    fileprivate struct Dimensions {
        static let footerHeight = CGFloat(50)
    }
}

class PhotosViewController: UICollectionViewController {
    private let refreshControl = UIRefreshControl()
    private weak var statusFooterView: SearchStatusView?
    var searchController : UISearchController!
    let viewModel = PhotosViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.alwaysBounceVertical = true
        collectionView.register(Nib.photo.nib,
                                forCellWithReuseIdentifier: Nib.photo.name)
        collectionView.register(Nib.statusFooter.nib,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
                                withReuseIdentifier: Nib.statusFooter.name)

        viewModel.actionStarted = { [weak self] in self?.handleAction(isCompleted: false, contentChanged: $0) }
        viewModel.actionCompleted = { [weak self] in self?.handleAction(isCompleted: true, contentChanged: $0) }

        refreshControl.addTarget(viewModel, action: #selector(viewModel.refresh), for: .valueChanged)
        refreshControl.tintColor = .white
        collectionView.refreshControl = refreshControl

        searchController = UISearchController(searchResultsController:  nil)
        searchController.searchResultsUpdater = self
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = viewModel.searchPlaceholderText
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.titleView = searchController.searchBar
        
        searchController.searchBar.becomeFirstResponder()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
         super.viewWillTransition(to: size, with: coordinator)
         // Have the collection view re-layout its cells.
         coordinator.animate(
             alongsideTransition: { _ in
                 self.collectionView.collectionViewLayout.invalidateLayout()
         },
             completion: { _ in }
         )
     }
}

extension PhotosViewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = viewModel.items[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Nib.photo.name, for: indexPath)
        (cell as? PhotoCollectionViewCell)?.setup(item: item)
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == viewModel.items.count - 1 {
            viewModel.loadNextPage()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionFooter) {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Nib.statusFooter.name, for: indexPath)
            if let statusFooterView = footerView as? SearchStatusView {
                statusFooterView.setup(viewModel: viewModel.searchStatus)
                self.statusFooterView = statusFooterView
            }
            return footerView
        }
        fatalError()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: Dimensions.footerHeight)
    }
}

extension PhotosViewController: UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        viewModel.updateSearch(term: searchController.searchBar.text ?? "")
    }
}

extension PhotosViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.bounds.size.width/3.0 - 1.0,height: self.view.bounds.size.width/3.0 - 1.0)
    }
}

extension PhotosViewController {
    private func handleAction(isCompleted: Bool, contentChanged: Bool) {
        if isCompleted && self.refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        }
        if contentChanged {
            self.collectionView.reloadData()
        } else {
            self.statusFooterView?.setup(viewModel: self.viewModel.searchStatus)
        }
    }
}
