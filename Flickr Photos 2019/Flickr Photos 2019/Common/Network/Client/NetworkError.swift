//
//  NetworkError.swift
//  Flickr Photos 2019
//
//  Created by Mitchell Currie on 5/1/20.
//  Copyright © 2020 Mitchell Currie. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case requestInvalid
    case noConnection
    case badResponse
}
