//
//  PhotosViewModel.swift
//  Flickr Photos 2019
//
//  Created by Mitchell Currie on 5/1/20.
//  Copyright © 2020 Mitchell Currie. All rights reserved.
//

import Foundation

enum LoadingState {
    case none(feedExhausted: Bool)
    case refresh
    case loading(page: Int)
}

extension LoadingState {
    var isRefresh: Bool {
        switch self {
        case .refresh: return true
        case .none, .loading(_): return false
        }
    }
    
    var pageIndexLoading: Int {
        switch self {
        case .loading(let page): return page
        case .refresh: return 1
        case .none: return 0
        }
    }
    
    var canLoadMore: Bool {
        switch self {
        case .none(let feedExhausted): return !feedExhausted
        case .refresh, .loading(_): return false  // Since is busy
        }
    }
}

class PhotosViewModel {
    let searchPlaceholderText = "Photo search term"
    var actionStarted: ((Bool)->())?  // Bool - did content change
    var actionCompleted: ((Bool)->())?  // Bool - did content change
    private let apiClient = SimpleClient(endpoint: .production)
    private(set) var items = [PhotoItem]()
    private var loadingState = LoadingState.none(feedExhausted: false)
    private var pages = 0
    private var term = ""
    private var errorOnLastOperation = false
    
    // reloads and starts from first page to get new content
    @objc func refresh() {
        guard !term.isEmpty else {
            self.actionCompleted?(false)
            return
        }
        updateSearch(term: term)
    }
    
    private func processResult(result: Result<PhotoSearchResponse, NetworkError>, term: String) {
        guard term == self.term else { return }  // Ignores results from earlier search terms that just came back
        switch result {
        case .success(let response):
            self.pages = self.loadingState.pageIndexLoading
            // Sometimes flickr doesn't return the number of items requested (generally less, it's weird)
            let newItems = response.photos.photo.map { PhotoItem(photo: $0) }
            // If a second page was loading, append it rather than assign it
            if self.loadingState.pageIndexLoading > 1 {
                self.items.append(contentsOf: newItems)
            } else {
                self.items = newItems
            }
            
            self.errorOnLastOperation = false
            self.actionCompleted?(true)
            self.loadingState = .none(feedExhausted: Int(response.photos.total) == self.items.count)
        case .failure(_):  // no ui to communicate error
            self.loadingState = .none(feedExhausted: false)  // If we tried in the first place, the feed isn't exhaisted
            self.errorOnLastOperation = true
            self.actionCompleted?(false)
        }
    }
    
    func updateSearch(term: String) {
        let isRefresh = (self.term == term)
        self.term = term
        self.pages = 0
        guard term.count > 0 else {
            items = []
            self.loadingState = .none(feedExhausted: false)
            self.actionCompleted?(true)
            return
        }
        
        loadingState = isRefresh ? LoadingState.refresh : LoadingState.loading(page: 1)
        // Clear items when term changes (only if needed)
        let invalidateLastSearch = !loadingState.isRefresh && !items.isEmpty
        items = invalidateLastSearch ? [] : items
        actionStarted?(invalidateLastSearch)

        apiClient.searchPhotos(searchTerm: term, page: loadingState.pageIndexLoading) { [weak self] result in
            self?.processResult(result: result, term: term)
        }
    }
    
    func loadNextPage() {
        guard loadingState.canLoadMore, !term.isEmpty else { return }
        
        loadingState = .loading(page: pages + 1)
        actionStarted?(false)
        apiClient.searchPhotos(searchTerm: term, page: loadingState.pageIndexLoading) { [weak self] result in
            guard let self = self else { return }
            self.processResult(result: result, term: self.term)
        }
    }
}

extension PhotosViewModel {
    var searchStatus: SearchStatusViewModel {
        switch (loadingState, errorOnLastOperation) {
        case (.none(_), true):
            let activityText = term.isEmpty ? "" : "Something went wrong"
            return SearchStatusViewModel(activityText: activityText, showsActivity: false)
        case (.none(true), _):
            let activityText = items.isEmpty ? "No results" : "That's all!"
            return SearchStatusViewModel(activityText: activityText, showsActivity: false)
        case (.none(false), _), (.refresh, _):
            return SearchStatusViewModel(activityText: "", showsActivity: false)
        case (.loading(let page), _):
            return SearchStatusViewModel(activityText: "Loading page \(page)", showsActivity: true)
        }
    }
}
