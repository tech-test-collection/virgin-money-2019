//
//  ViewModelTests.swift
//  Flickr Photos 2019Tests
//
//  Created by Mitchell Currie on 5/1/20.
//  Copyright © 2020 Mitchell Currie. All rights reserved.
//

import XCTest

class ViewModelTests: XCTestCase {
    override func setUp() {
        setupStubsDefault()
    }
    
    func testEmpty() {
        let viewModel = PhotosViewModel()
        XCTAssertEqual(viewModel.items.count, 0)
    }
    
    func testFirstPage() {
        let viewModel = PhotosViewModel()
        let beginExpectation = expectation(description: "testAPI.start")
        let completionExpectation = expectation(description: "testAPI.complete")
        
        viewModel.actionStarted = { contentChanged in
            beginExpectation.fulfill()
            XCTAssertFalse(contentChanged)
            XCTAssertTrue(viewModel.searchStatus.showsActivity)
            XCTAssertEqual("Loading page 1", viewModel.searchStatus.activityText)
        }
        
        viewModel.actionCompleted = { contentChanged in
            completionExpectation.fulfill()
            XCTAssertTrue(contentChanged)
            XCTAssertEqual(viewModel.items.count, 95)
        }
        
        viewModel.updateSearch(term: "alligator")
        
        waitForExpectations(timeout: .waitExpectation) { error in
            XCTAssertNil(error, "Time out error")
        }
    }
    
    func testNextPage() {
        let viewModel = PhotosViewModel()
        let beginExpectation = expectation(description: "testAPI.start")
        let completionExpectation = expectation(description: "testAPI")
        
        var count = 0
        viewModel.actionCompleted = { contentChanged in
            XCTAssertTrue(contentChanged)
            count += 1
            if count == 1 {
                DispatchQueue.main.asyncAfter(deadline: .now() + TimeInterval.dispatch, execute: {
                    viewModel.actionStarted = { contentChanged in
                        beginExpectation.fulfill()
                        XCTAssertFalse(contentChanged)
                        XCTAssertTrue(viewModel.searchStatus.showsActivity)
                        XCTAssertEqual("Loading page 2", viewModel.searchStatus.activityText)
                    }
                    viewModel.loadNextPage()
                })
            } else {
                completionExpectation.fulfill()
                XCTAssertEqual(viewModel.items.last?.title, "DSC_0111")
            }
        }
        
        viewModel.updateSearch(term: "alligator")
        
        waitForExpectations(timeout: .waitExpectation) { error in
            XCTAssertNil(error, "Time out error")
        }
    }
    
    func testClearSearchTerm() {
        let viewModel = PhotosViewModel()
        let completionExpectation = expectation(description: "testAPI")
        
        var count = 0
        viewModel.actionCompleted = { contentChanged in
            XCTAssertTrue(contentChanged)
            count += 1
            if count == 1 {
                viewModel.updateSearch(term: "")
            } else {
                completionExpectation.fulfill()
                XCTAssertTrue(viewModel.items.isEmpty)
            }
        }
        
        viewModel.updateSearch(term: "alligator")
        
        waitForExpectations(timeout: .waitExpectation) { error in
            XCTAssertNil(error, "Time out error")
        }
    }
    
    func testRefreshSearchTerm() {
        let viewModel = PhotosViewModel()
        let beginExpectation = expectation(description: "testAPI.start")
        let completionExpectation = expectation(description: "testAPI")
        
        var count = 0
        viewModel.actionCompleted = { contentChanged in
            XCTAssertTrue(contentChanged)
            count += 1
            if count == 1 {
                viewModel.actionStarted = { contentChanged in
                    beginExpectation.fulfill()
                    XCTAssertFalse(contentChanged)
                    XCTAssertFalse(viewModel.searchStatus.showsActivity)
                    XCTAssertEqual("", viewModel.searchStatus.activityText)
                    XCTAssertFalse(viewModel.items.isEmpty)

                }
                viewModel.updateSearch(term: "alligator")
            } else {
                completionExpectation.fulfill()
                XCTAssertFalse(viewModel.items.isEmpty)
            }
        }
        
        viewModel.updateSearch(term: "alligator")
        
        waitForExpectations(timeout: .waitExpectation) { error in
            XCTAssertNil(error, "Time out error")
        }
    }
    
    func testChangeSearchTerm() {
        let viewModel = PhotosViewModel()
        let beginExpectation = expectation(description: "testAPI.start")
        let completionExpectation = expectation(description: "testAPI")
        
        var count = 0
        viewModel.actionCompleted = { contentChanged in
            XCTAssertTrue(contentChanged)
            count += 1
            if count == 1 {
                viewModel.actionStarted = { contentChanged in
                    beginExpectation.fulfill()
                    XCTAssertTrue(contentChanged)
                    XCTAssertTrue(viewModel.searchStatus.showsActivity)
                    XCTAssertEqual("Loading page 1", viewModel.searchStatus.activityText)
                    XCTAssertTrue(viewModel.items.isEmpty)
                }
                viewModel.updateSearch(term: "kittens")
            } else {
                completionExpectation.fulfill()
                XCTAssertFalse(viewModel.items.isEmpty)
            }
        }
        
        viewModel.updateSearch(term: "alligator")
        
        waitForExpectations(timeout: .waitExpectation) { error in
            XCTAssertNil(error, "Time out error")
        }
    }
    
    func testError() {
        setupStubError()
        
        let viewModel = PhotosViewModel()
        let beginExpectation = expectation(description: "testAPI.start")
        let completionExpectation = expectation(description: "testAPI.complete")
        
        viewModel.actionStarted = { contentChanged in
            beginExpectation.fulfill()
            XCTAssertFalse(contentChanged)
            XCTAssertTrue(viewModel.searchStatus.showsActivity)
            XCTAssertEqual("Loading page 1", viewModel.searchStatus.activityText)
        }
        
        viewModel.actionCompleted = { contentChanged in
            completionExpectation.fulfill()
            XCTAssertFalse(contentChanged)
            XCTAssertTrue(viewModel.items.isEmpty)
            XCTAssertEqual("Something went wrong", viewModel.searchStatus.activityText)
        }
        
        viewModel.updateSearch(term: "alligator")
        
        waitForExpectations(timeout: .waitExpectation) { error in
            XCTAssertNil(error, "Time out error")
        }
    }
}
